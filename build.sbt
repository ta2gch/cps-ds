import Dependencies._

ThisBuild / scalaVersion     := "2.13.1"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "casa.docs"

lazy val root = (project in file("."))
  .settings(
    name := "fast",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      "org.scala-lang" % "scala-reflect" % "2.13.1",
      "org.typelevel" %% "cats-core" % "2.0.0",
      "org.typelevel" %% "cats-free" % "2.0.0",
      "org.tpolecat" %% "atto-core" % "0.7.1"
    )
  )

// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
