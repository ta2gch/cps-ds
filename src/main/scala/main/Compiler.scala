package fast
import cats._, implicits._
import atto._, Atto._

object Compiler {
  def compile(e: Expression): Array[Operation] = {
    def compile(e: Expression)(i: Int): (Int, List[Operation]) = {
      e match {
        case ListExp(StrExp("F"), StrExp(x), y) =>
          val (j, t) = compile(y)(i)
          (j + 1, t :+ F(Operation.stoi(x), j - 1))
        case ListExp(StrExp("A"), x, y) =>
          val (j, t) = compile(x)(i)
          val (k, u) = compile(y)(j)
          (k + 1, t ++ u :+ A(j - 1, k - 1))
        case ListExp(StrExp("S"), StrExp(x)) =>
          (i + 1, List(S(Operation.stoi(x), 0)))
      }
    }
    val (i, t) = compile(e)(0)
    Array.from(t :+ T(i - 1, 0))
  }
}
