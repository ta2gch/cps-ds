package fast
import cats._, implicits._
import scala.language.experimental.macros

sealed trait Expression
final case class StrExp(value: String) extends Expression
final case class ListExp(value: Expression*) extends Expression

object Expression {
  implicit object ExpressionShow extends Show[Expression] {
    def show(t: Expression): String = {
      t match {
        case ListExp(value @ _*) => value.map(_.show).mkString("(", " ", ")")
        case StrExp(value)       => value
      }
    }
  }
}
